<div class="vw-100 vh-100 fullPageTemplate position-relative" style="background-size:cover; background-position: center center; background-repeat: no-repeat; background-image: url('<?=$siteUrl.$imagesPath.$sections[($page-1)]['background']?>')">
  <div class="sectionContentOverlay"></div>
  <div class="flex-wrapper">
    <div class="align-self-center w-100">
      <div class="fullPageContent text-center">
        <img src="<?=$imagesPath?>geografico-logo.png" alt="" style="height: 60px; margin-bottom: 20px">
        <?php
        if(isset($fullPageCfg['preIcons'])){
          ?>
          <div class="fullPageContentPreIcons text-center">
            <div class="d-inline-block w-75">
              <div class="row">
                <?php foreach($fullPageCfg['preIcons'] as $preIcon){
                  ?>
                  <div class="col text-uppercase text-center mb-2">
                    <a class="text-white" href="<?=$preIcon['href']?>"><img class="icon-small" src="<?=$imagesPath?>icons8-<?=$preIcon['icon']?>.png" alt=""> | <span class="text-small"><?=$preIcon['title']?></span></a>
                  </div>
                <?php }?>
              </div>
            </div>

          </div>
          <?php
        }
        ?>

        <div class=" text-center">
          <div class="d-inline-block w-75">
            <h1 class="text-center text-gold font-serif text-uppercase mt-4"><?=$fullPageCfg['title']?></h1>
            <p class="fullPageDescription text-center text-white font-serif"><?=$fullPageCfg['description']?></p>
          </div>
        </div>
        <?php
        if(isset($fullPageCfg['middleIcon'])){
          ?>
          <div class="middleIcon text-center">
            <img src="<?=$imagesPath?>icons8-<?=$fullPageCfg['middleIcon']['icon']?>.png" alt="">
            <p class="text-center text-uppercase text-white text-small mt-1"><?=$fullPageCfg['middleIcon']['description']?></p>
          </div>
          <?php
        }else{
          if(isset($fullPageCfg['noArrowDown'])){
            if(!$fullPageCfg['noArrowDown']){
              ?>
              <?php include 'commons/arrowScrollDown.php'; ?>
              <?php
            }
          }else{
            ?>
            <?php include 'commons/arrowScrollDown.php'; ?>
            <?php
          }
        }

        if(isset($fullPageCfg['buttons'])){?>
          <div class="text-center" style="margin-top: 50px;">
            <div class="d-inline-block w-75">
              <?php
              foreach($fullPageCfg['buttons'] as $button){
                if(!isset($button['external_link'])){
                ?>
                <a href="javascript:" onclick="scrollToDiv('<?=$button['href']?>')" class="text-center text-uppercase text-white border-white border d-block p-2 text-small text-nowrap">
                  <img class="icon-small" src="<?=$imagesPath?>icons8-<?=$button['icon']?>.png" alt="">
                  <?=$button['title']?>
                </a>
                <br>
                <?php }
                else{
                  if($button['external_link'] == true){
                    ?>
                    <a href="<?=$button['href']?>" class="text-center text-uppercase text-white border-white border d-block p-2 text-small text-nowrap">
                      <img class="icon-small" src="<?=$imagesPath?>icons8-<?=$button['icon']?>.png" alt="">
                      <?=$button['title']?>
                    </a>
                    <br>
                    <?php
                  }
                }
              }?>
            </div>
          </div>
          <?php
        }else{
          if(isset($fullPageCfg['bottomColumns'])){
            ?>
            <div class="fullPageBottomColumnsr container-fluid">
              <div class="row">
                <?php
                foreach($fullPageCfg['bottomColumns'] as $col){
                  ?>
                  <div class="col text-center">
                    <h2 class="subtitle font-serif text-white text-uppercase"><?=$col['title']?></h2>
                    <?php include 'commons/linksList.php'?>
                  </div>
                  <?php
                }?>

              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
    </div>
  </div>
</div>
