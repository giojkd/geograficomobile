<div class="vh-100 w-100 position-relative" id="fullPageGallery-<?=$gallery['index']?>">
  <div class="slickGalleryHeader">
    <img src="<?=$imagesPath?>geografico-logo.png" alt="" style="height: 60px">
  </div>

  <div class="slick-carousel">

    <?php foreach($gallery['photos'] as $index => $photo){
      ?>
      <div class="vh-100 w-100" style="background-size:cover; background-position: center center; background-repeat: no-repeat; background-image:url('<?=$imagesPath.$photo?>')"></div>
    <?php }?>

  </div>
  <div class="slickGalleryFooter">
    <?=$gallery['title']?>
  </div>
</div>
