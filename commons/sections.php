<?php

$sections = [
  [
    'title' => 'Storia',
    'subtitle' => 'Un sogno lungo<br>58 anni',
    'index' => 0,
    'background' => '1-storia.jpg',
  ],
  [
    'title' => 'Cantine',
    'subtitle' => 'San Gimignano<br>& Gaiole',
    'index' => 1,
    'background' => '2-le-nostre-cantine.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => '',
        'extraClasses' => ''
      ],
      [
        'index' => 1,
        'background' => 'timelineSlide',
        'extraClasses' => 'timelineSlide'
      ],
      [
        'index' => 2,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100 '
      ],
      [
        'index' => 3,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100'
      ],
      [
        'index' => 4,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100'
      ]
    ]
  ],
  [
    'title' => 'Vini & Enoteche',
    'subtitle' => 'Gallery<br>Wine & Shop',
    'index' => 2,
    'background' => '3-vini-e-enoteche.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => 'Section2Bg.png',
        'extraClasses' => 'd-flex h-100 backgroundOverlay'
      ],
      [
        'index' => 1,
        'background' => 'timelineSlide',
        'extraClasses' => 'stdPage d-flex h-100 '
      ],
      [
        'index' => 2,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100 '
      ],
      [
        'index' => 3,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'galleryPage d-flex h-100'
      ],
      [
        'index' => 4,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'galleryPage d-flex h-100'
      ]
    ]
  ],
  [
    'title' => 'Contessa di Radda',
    'subtitle' => 'Vigneto Toscano<br>Prestigioso dal 1500',
    'index' => 3,
    'background' => '4-contessa-di-radda.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => 'slide3bg.png',
        'extraClasses' => 'd-flex h-100 backgroundOverlay',

      ],
      [
        'index' => 1,
        'background' => 'timelineSlide',
        'extraClasses' => 'productPage d-flex h-100 ',
        'title' => 'Schede vini'
      ],
      [
        'index' => 2,

        'extraClasses' => 'blogPage d-flex h-100 '
      ]
    ]
  ],
  [
    'title' => 'IFIS',
    'subtitle' => 'Progetto di filiera',
    'index' => 4,
    'background' => '5-progetto-di-filiera.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => 'sangimignano_wine_chianti_tuscany_1.png',
        'extraClasses' => 'd-flex h-100 backgroundOverlay'
      ],
      [
        'index' => 1,
        'extraClasses' => 'stdPage  d-flex h-100 '
      ],
      [
        'index' => 2,
        'extraClasses' => 'galleryPage d-flex h-100 '
      ]
    ]
  ]
];

?>
