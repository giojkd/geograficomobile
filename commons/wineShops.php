<?php

$wineShops = [
  [
    'name' => 'Geografico Wine Shop San Gimignano',
    'openingTime' => 'Aperto tutti i giorni dal Lunedì alla Domenica dalle 10:30 alle 18:30',
    'description' => 'Qui potrete degustare una vasta selezione dei vini del CHIANTI GEOGRAFICO e delle TENUTE PICCINI,<br>dai classici di San Gimignano fino ai vini vulcanici dell’Etna,<br>passando dalle pregiate zone del Chianti Classico, Montalcino, Maremma e Vulture.<br>Il nostro personale sarà lieto di guidarvi in degustazioni.<br>Per gruppi di oltre 10 persone è consigliabile avvisare via email o telefono per organizzare il menu. ',
    'location' => 'Loc. Casa alla Terra, 39 - San Gimignano (SI)',
    'email' => 'wineshop@chiantigeografico.it',
    'telephone' => '+39 0577 988262',
    'gallery' => 'san_gimignano'
  ],
  [
    'name' => 'Vendita Diretta Gaiole in Chianti',
    'openingTime' => 'Da lunedì a sabato | 09:00 - 13:00 e 14:00 - 18:00',
    'description' => 'Troverete la linea completa dei vini del Chianti Geografico.<br>Il negozio offre anche la possibilità di acquistare vino sfuso di ottima qualità attraverso un tradizionale erogatore.<br>Il nostro personale sarà lieto di guidarvi in degustazioni. ',
    'location' => 'Via Mulinaccio, 10 - Gaiole in Chianti (SI)',
    'email' => 'shop.gaiole@chiantigeografico.it',
    'telephone' => '+39 0577 749413',
    'gallery' => 'gaiole_in_chianti'
  ]
];

?>

<hr>

<div class="vh-100 vw-100" id="wineShops">

  <?php
  foreach($wineShops as $shop){
    ?>
    <div class="wineShop text-center justify-content-center align-self-center">

      <div class="row vw-100p">
        <div class="col-md-12 text-center">
          <h1><?=$shop['name']?></h1>
          <h2><?=$shop['openingTime']?></h2>
          <div class="littleSeparator"></div>
          <p><?=$shop['description']?></p>
        </div>
      </div>
      <div class="vw-100p topBar container-fluid">
        <div class="row">
          <div class="col">
            <ul class="list list-unstyled">
              <li class="mb-2 text-center"><a href="<?=$spsl_default['gallery']?>">
                <img src="<?=$imagesPath?>icons8-gallery.png" alt=""> | Gallery
              </a></li>
              <li class="mb-2 text-center">  <a href="<?=$spsl_default['wine_cards']?>">
                  <img src="<?=$imagesPath?>icons8-sim_card.png" alt=""> | Schede Vini
                </a></li>
              <li class="mb-2 text-center">  <a href="<?=$spsl_default['wine_shop']?>">
                  <img src="<?=$imagesPath?>icons8-add_shopping_cart.png" alt=""> | Shop Vini
                </a></li>
              <li class="mb-2 text-center">
                <a href="mailto:<?=$shop['email']?>">
                    <img src="<?=$imagesPath?>icons8-bowl_with_spoon.png" alt=""> | Prenota degustazione
                  </a></li>
            </ul>
          </div>
        </div>


      </div>
      <div class="row vw-100p bottomBar container-fluid">
        <div class="col-12">
          <div class=" mb-1 text-center">
            <a target="_blank" href="http://maps.google.com/?q=<?=urlencode($shop['location'])?>">
              <img src="<?=$imagesPath?>icons8-address.png" alt=""> <?=$shop['location']?>
            </a>
          </div>

        </div>
        <div class="col-12 mb-1">
          <div class=" mb-1 text-center">
            <a href="mailto:<?=$shop['email']?>">
              <img src="<?=$imagesPath?>icons8-mailing.png" alt=""> <?=$shop['email']?>
            </a>
          </div>
        </div>
        <div class="col-12">
          <div class=" mb-1 text-center">
            <a href="tel:<?=$shop['telephone']?>"><img src="<?=$imagesPath?>icons8-number_pad.png" alt=""> <?=$shop['telephone']?></a>
          </div>
        </div>
      </div>
    </div>
  <?php }
  ?>
</div>
