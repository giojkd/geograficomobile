<div class="pageIconsListWrapper text-left row" >

  <div class="col">
    <ul class="stdPageIconsList list list-unstyled text-left <?=(isset($fullPageCfg['bottomColumnsExtraClasses'])) ? $fullPageCfg['bottomColumnsExtraClasses'] : ''?>">
      <li>
        <a href="<?=$spsl_default['gallery']?>">
          <img class="icon-small icon-list" src="<?=$imagesPath?>icons8-gallery.png" alt=""> <span class="text-small text-uppercase">| Gallery</span>
        </a>
      </li>
      <li>
        <a href="<?=$spsl_default['wine_cards']?>">
          <img class="icon-small icon-list" src="<?=$imagesPath?>icons8-sim_card.png" alt=""> <span class="text-small text-uppercase"> | Schede Vini</span>
        </a>
      </li>
      <li>
        <a href="<?=$spsl_default['wine_shop']?>">
          <img class="icon-small icon-list" src="<?=$imagesPath?>icons8-add_shopping_cart.png" alt=""> <span class="text-small text-uppercase"> | Shop Vini</span>
        </a>
      </li>
      <li>
        <a href="<?=$spsl_default['reserve_tasting']?>">
          <img class="icon-small icon-list" src="<?=$imagesPath?>icons8-bowl_with_spoon.png" alt=""> <span class="text-small text-uppercase"> | Prenota Degustazione</span>
        </a>
      </li>
    </ul>
  </div>


</div>
