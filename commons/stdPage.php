<?php $showStdPageIconLinks = (isset($showStdPageIconLinks)) ? $showStdPageIconLinks : true; ?>
<div class="text-center" id="stdPage-<?=$pageContent['index']?>">

  <div class="w-75 d-inline-block">


    <div class="justify-content-center align-self-center">


      <div id="slider-<?=$page?>-<?=$pageContent['index']?>" class="carousel slide stdPageCarousel mt-4" data-ride="carousel">
        <div class="carousel-inner">
          <?php foreach($pageContent['photos'] as $photoIndex => $photo){?>
            <div class="carousel-item <?=($photoIndex == 0) ? 'active' : ''?>">
              <img src="<?=$imagesPath.$photo?>" class="d-block w-100" alt="...">
            </div>
          <?php }?>
        </div>

          <div class="stdPageCarouselControls" style="opacity:<?=($showStdPageIconLinks) ? '1' : '0';?>">
            <a href="#slider-<?=$page?>-<?=$pageContent['index']?>" role="button" data-slide="prev">
              <i class="fa fa-angle-left"></i>
            </a>
            <a href="#slider-<?=$page?>-<?=$pageContent['index']?>" role="button" data-slide="next">
              <i class="fa fa-angle-right"></i>
            </a>

        </div>

      </div>


      <div class="text-center">
        <p  class="stdPageTitleParagraph text-gold font-serif mb-1 mt-4"><?=$pageContent['superTitle']?></p>
        <?php if(isset($pageContent['title'])){?>
          <h1 class="font-serif text-uppercase"><?=$pageContent['title']?></h1>
        <?php } ?>
        <p><?=$pageContent['subtitle']?></p>
        <div class="content font-serif">
          <?php foreach($pageContent['description'] as $descr){?>
            <?=(isset($descr['title'])) ? '<h3>'.$descr['title'].'</h3>' : '' ?>
            <p><?=$descr['content']?></p>
          <?php } ?>
        </div>
        <hr>
        <?php

        if($showStdPageIconLinks){
          include 'commons/linksList.php';

        }?>
      </div>
    </div>
  </div>
</div>

<div class="" style="height: 40px;"></div>
