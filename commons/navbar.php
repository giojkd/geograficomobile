<div class="fixed-top w-100 geografioMobileNavbar">
  <div class="row">
    <div class="col">
      <div class="pl-3 pt-3">
        <a href="javascript:" class="text-white" onclick="$('.slidingMenu').removeClass('out')">
          <i class="fa fa-bars"></i>
          <span class="text-uppercase text-small ml-2">Home</span>
        </a>
      </div>
    </div>
    <div class="col text-right">
      <div class="pr-3 pt-3">
        <a href="#" class="text-white mr-2"> <i class="fab fa-instagram" aria-hidden></i> </a>
        <a href="#" class="text-white mr-2"> <i class="fab fa-facebook-square" aria-hidden></i> </a>
        <!--<a href="#" class="text-white"> <i class="fab fa-youtube" aria-hidden></i> </a>-->
      </div>
    </div>
  </div>
</div>
