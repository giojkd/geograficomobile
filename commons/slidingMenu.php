<div class="slidingMenu out vh-100" onclick="$(this).addClass('out')">
  <div class="row">

    <div class="col">
      <div class="d-flex vh-100">
        <div class="slidingMenuContent align-self-center">
          <ul class="list list-unstyled">
            <?php
            $sectionsMenu = array_merge(
              [

                [
                  'title' => 'Home',
                  'subtitle' => 'Scopri il Chianti Geografico',
                  'index' => -1,
                  'background' => 'PicciniVigneto.png',
                ]
              ],

              $sections,[
            /*  [
                'title' => 'Experience',
                'subtitle' => 'Video Emozionale',
                'index' => 1,
                'background' => 'PicciniVigneto.png',
              ]*/
            ],[
              [
                    'title' => 'Shop',
                    'subtitle' => 'Acquista i nostri vini',
                    'index' => 3,
                    'slide' => 2,
                    'background' => 'PicciniVigneto.png',
                    'href' => $spsl_default['wine_shop'],
                ],
              [
              'title' => 'Contatti',
              'subtitle' => 'Vieni a trovarci',
              'index' => 2,
              'background' => 'PicciniVigneto.png',
              'div' => 'wineShops'
            ]
          ]);
            foreach($sectionsMenu as $section){

              if(isset($section['href'])){
                ?>
                <li onclick="location.replace('<?=$section['href']?>')">
                <?php
              }else{
              ?>
              <li onclick="location.replace('<?=$siteUrl?>?page=<?=($section['index']+1)?><?=(isset($section['div'])) ? '#'.$section['div'] : ''?>')">
              <?php }?>
                <strong><?=$section['title']?></strong>
                <span><?=str_replace('<br>',' ',$section['subtitle'])?></span>
              </li>
            <?php }
            ?>
          </ul>



        </div>
      </div>
        <div class="close"></div>
    </div>
  </div>
</div>
