<script type="text/javascript">
var products = <?=json_encode($products)?>;
var currentProduct  = <?=json_encode($products[0]['products'][0])?>
</script>
<div class="bg-grey-medium" id="productsPage">
  <div class="" data-toggle="collapse" href="#winesListAccordion" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">


    <h1 class="text-gold text-uppercase font-serif text-center pt-4">I nostri vini</h1>
    <div class="text-center">
      <a style="color: black!important" > <i class="fas fa-angle-double-down" aria-hidden></i> </a>
    </div>
  </div>

  <hr>
  <div class="accordion collapse" id="winesListAccordion">
    <?php
    $productCount = 0;
    foreach($products as $index => $product){ ?>
      <div class="card" data-product_card="<?=$index?>" >
        <div class="card-header" id="wines-list-heading-<?=$index?>">
          <h2 class="mb-0">
            <button class="btn text-nowrap btn-link <?=($index == 0) ? '' : 'collapsed'?>" type="button" data-toggle="collapse" data-target="#wines-list-<?=$index?>" aria-expanded="<?=($index == 0) ? 'true' : 'false'?>" aria-controls="wines-list-<?=$index?>">
              <span class="wineListCaret <?=($index == 0) ? 'open' : ''?>"><i class="fas fa-angle-right"></i></span>  <?=$product['category_name']?> (<?=count($product['products'])?>)
            </button>
          </h2>
        </div>
        <div id="wines-list-<?=$index?>" class="collapse card-body-wrapper <?=($index == 0) ? 'show' : ''?>" aria-labelledby="wines-list-heading-<?=$index?>" data-parent="#winesListAccordion">
          <div class="card-body">
            <ul class="list list-unstyled">
              <?php foreach($product['products'] as $item){?>
                <li class="wineSelect text-nowrap <?=($productCount == 0) ? 'active' : ''?>" id="wineSelect-<?=$productCount?>" onclick="changeWine(<?=$productCount?>); $('.wineSelect').removeClass('active'); $(this).addClass('active')"><?=$item['name']?></li>
                <?php
                $productCount++;
              } ?>
            </ul>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
<script type="text/javascript">
var countProducts = <?=$productCount?>;
</script>
<div class="text-center" id="productsCarouselWrapper">
  <div class="w-75 d-inline-block">
    <div id="productsCarousel" class="carousel slide stdPageCarousel mt-4" data-ride="carousel" data-interval="false">
      <ol class="carousel-indicators d-none">
        <?php
        $productCount = 0;
        foreach($products as $productCategories){
          foreach($productCategories['products'] as  $product){?>
            <li id="productsCarouselIndicator-<?=$productCount?>" data-target="#productsCarousel" data-slide-to="<?=$productCount?>" class="<?=($productCount == 0) ? 'active' : ''?>"></li>
            <?php
            $productCount++;

          }
        }?>
      </ol>
      <div class="carousel-inner">
        <?php
        $productCount = 0;
        foreach($products as $productCategories){
          foreach($productCategories['products'] as  $product){?>
            <div class="carousel-item text-center <?=($productCount == 0) ? 'active' : ''?>">
              <img src="<?=$imagesPath?>vino_<?=$productCount?>.png" class="py-4 d-inline-block w-50" alt="...">
            </div>
            <?php
            $productCount++;
          }
        }?>
      </div>
      <div class="stdPageCarouselControls">
        <a href="#productsCarousel" role="button" data-slide="prev">
          <i class="fa fa-angle-left"></i>
        </a>
        <a href="#productsCarousel" role="button" data-slide="next">
          <i class="fa fa-angle-right"></i>
        </a>
      </div>
    </div>
    <div class="col">
      <div style="height: 15px;"></div>
      <div class="pageIconsListWrapper text-left row">

        <div class="col">
          <ul class="stdPageIconsList list list-unstyled text-left <?=(isset($fullPageCfg['bottomColumnsExtraClasses'])) ? $fullPageCfg['bottomColumnsExtraClasses'] : ''?>">
            <li>
              <a download id="downloadTechnicalSheet" href="https://www.chiantigeografico.it/asset/schede_vini/<?=$products[0]['products'][0]['ts']?>.pdf">
                <img class="icon-small icon-list" src="<?=$imagesPath?>icons8-pdf.png" height="20" alt=""> <span class="text-small text-uppercase">| Scheda prodotto</span>
              </a>
            </li>
          </ul>
        </div>

      </div>

      <?php include 'commons/linksList.php';?>
      <?php /*
      <ul class="list-unstyled text-left mt-4">
      <li class="text-uppercase mb-2">
      <a href="<?=$spsl_default['wine_shop']?>">
      <img class="icon-small" src="<?=$imagesPath?>icons8-add_shopping_cart_black.png" alt="">

      <span class="text-small">Vai allo shop</span>
      </a>
      </li>
      <!--<li class="text-uppercase mb-2"><img class="icon-small" src="<?=$imagesPath?>icons8-pdf.png" alt=""> <span class="text-small">Scarica la scheda prodotto</span></li>-->
      <li class="text-uppercase mb-2">
      <a href="<?=$spsl_default['reserve_tasting']?>">
      <img class="icon-small" src="<?=$imagesPath?>icons8-bowl-black.png" alt=""> <span class="text-small">Prenota la tua degustazione</span>
      </a>
      </li>
      </ul>*/?>
    </div>
  </div>
