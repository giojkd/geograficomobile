<?php
$fullPageCfg = [
 'preIcons' => [
   [
     'icon' => 'gallery',
     'title' => 'Gallery',
     'href' =>$spsl_default['gallery']
   ],
   [
     'icon' => 'add_shopping_cart',
     'title' => 'Shop',
     'href' =>$spsl_default['wine_shop']
   ]
 ],
 'title' => 'Contessa di Radda',
 'description' => '',

 'buttons' => [
   [
     'title' => 'Storia',
     'icon' => 'activity_history',
     'href' => '#stdPage-1'
   ],
   [
     'title' => 'I nostri vini',
     'icon' => 'grapes-small',
     'href' => $siteUrl.'?page=3#productsPage',
     'external_link' => true
   ],
   [
     'title' => 'Enoteche',
     'icon' => 'bowl_with_spoon',
     'href' => $siteUrl.'?page=3#wineShops',
     'external_link' => true
   ],
 ]
];

$images = 25;
$gallery = [];
for($i = 1 ; $i <= $images; $i ++){
  $gallery[] = 'gallery_vertical_contessa_di_radda/Img_'.$i.'.jpg';
}
shuffle($gallery);
$pageContent = [
  'index' => 1,
  'superTitle' => 'Storia',
  'title' => 'CONTESSA DI RADDA',
  'subtitle' => 'Un universo di vini che ruota attorno al vitigno principe toscano, il Sangiovese.',
  'description' => [

    [
      'title' => 'La storia del Chianti Classico Contessa di Radda è intrisa di tradizione e cultura, oltre ad essere fortemente legata al suo territorio di appartenenza.',
      'content' => 'Il Chianti Classico Contessa di Radda, nasce nel 1978 e prende il suo nome dalla contessa Willa di Toscana, madre del celebre Marchese Ugo di Toscana, citato dallo stesso Dante Alighieri nella Divina Commedia come il “Gran Barone”. Lei che, nel 978, fu fondatrice del più antico monastero benedettino di Firenze, l’Abbazia di Santa Maria, meglio conosciuta come Badia Fiorentina.'
    ],
    [
      'content' => 'Il villaggio di Radda in Chianti, che divenne poco dopo capoluogo storico della Lega del Chianti, fu citato per la prima volta in un documento risalente al 1002. Attraverso questo diploma, infatti, l’Imperatore, del Sacro Romano Impero, Ottone III confermava la donazione effettuata proprio dalla contessa Willa di alcuni dei territori del Chianti, tra cui Radda, in favore della Badia Fiorentina. A seguito di questo primo attestato, la località di Radda apparirà in numerosi altri documenti dell’Abbazia di Santa Maria di Firenze, fino al XII secolo. Questo a dimostrazione del grande valore strategico rappresentato dalla zona.'
    ],
    [

      'content' => 'Proprio per questo motivo, gli Agricoltori del Chianti Geografico hanno deciso di scegliere lei come volto-icona di una delle etichette più emblematiche del Chianti Classico.'
    ]
  ],
  'photos' => $gallery
];
?>

<?php include 'commons/fullpage.php'; ?>
<?php include 'commons/stdPage.php'; ?>

<?php
shuffle($gallery);
$gallery1 = $gallery;
$gallery = [
  'photos' => $gallery1,
  'title' => 'San Giovese'
];

?>

<?php #include 'commons/fullPageGallery.php' ?>
