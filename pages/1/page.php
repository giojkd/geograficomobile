<?php


$images = 31;
$gallery = [];
for($i = 1 ; $i <= $images; $i ++){
  $gallery[] = 'gallery_vertical/Img_'.$i.'.jpg';
}
shuffle($gallery);
$fullPageCfg = [
  'preIcons' => [
    [
      'icon' => 'add_shopping_cart',
      'title' => 'Shop',
      'href' =>$spsl_default['wine_shop']
    ]
  ],
  'title' => 'La storia di geografico',
  'description' => '',
  'buttons' => [
    [
      'title' => 'La Storia',
      'icon' => 'activity_history',
      'href' => '#stdPage-1'
    ],
    [
      'title' => 'Il Territorio',
      'icon' => 'grapes-small',
      'href' => '#stdPage-2'
    ],
    [
      'title' => 'Mission and Vision',
      'icon' => 'bowl_with_spoon',
      'href' => '#stdPage-3'
    ],
  ]
];


$pageContent = [
  'index' => 1,
  'superTitle' => 'Storia',
  'title' => 'Il Geografico, La Storia del Chianti',
  'subtitle' => 'Questa è una storia che parla di vino, di territorio, di identità e di solidarietà',
  'description' => [
    [
      'title' => '',
      'content' => 'Nella straordinaria storia del Chianti vi sono alcuni protagonisti  che più di altri hanno avuto il merito e la responsabilità di rendere leggendario questo vino, anche attraverso una sincera valorizzazione del suo territorio di produzione, unico per una grande varietà di aspetti.'
    ],
    [
      'content' => 'Oggi, dire Chianti nel mondo, significa evocare molto di più di un vino. Questo è stato reso possibile anche grazie a diciassette viticoltori lungimiranti, - “Quelli del Geografico” - che già nel 1961 capirono l’importanza dello straordinario rapporto tra un vino e il suo territorio di produzione.'

    ],
    [
      'content' => 'Proprio qui, più di mezzo secolo fa, nacque la storia del Geografico, che oggi prosegue il suo cammino grazie alla passione per il territorio di Tenute Piccini, che lo ha acquistato per far proseguire la sua memoria.
      Da quei diciassette viticoltori iniziali, oggi ne possiamo contare ben centosettanta. Tutti prevalentemente radicati nei territori del Chianti, Chianti Classico, ma anche della Vernaccia di San Gimignano. Centosettanta appassionati che lavorano ogni giorno per valorizzare una grande denominazione e i suoi vini, a partire dal suo vitigno principe: il Sangiovese.'  ]
    ],
    'photos' => $gallery
  ];
  ?>

  <div class="position-relative" id="videoWrapper">
    <video poster="<?=$siteUrl.$imagesPath?>video-poster.png" style="height: 100%; object-fit: fill;" class="w-100" controls autoplay playsinline type="video/mp4" id="geograficoVideo">
      <source src="assets/videos/video-geografico-mobile.mp4" >
        Your browser does not support the video tag.
      </video>
    </div>



    <!-- FULLPAGE -->
    <?php include 'commons/fullpage.php'; ?>

    <div class="text-center">
      <h1 class="text-gold text-uppercase font-serif my-4">Timeline</h1>
      <div class="w-100 text-cent d-inline-block" style="padding-left: 10px">
        <img src="<?=$imagesPath?>timeline.png" alt="" class="img-fluid">
      </div>

      <img src="<?=$imagesPath?>Toscana.png" alt="" class="img-fluid">
    </div>


    <!-- STDPAGE -->
    <?php include 'commons/stdPage.php'; ?>

    <?php
    shuffle($gallery);
    $pageContent = [
      'index' => 2,
      'superTitle' => 'Territorio',
      'title' => 'Le nostre colline<br> il nostro vanto',
      'subtitle' => 'Questa è una storia che parla di vino, di territorio, di identità e di solidarietà',
      'description' => [
        [
          'content' => 'I vigneti dei centosettanta soci del Geografico sono dislocati nelle zone di produzione più prestigiose della Toscana, interessando una superficie pari a circa 550 ettari.
          Inoltre, più di 400 di questi ettari sono iscritti in Albi di produzioni
          a Denominazione di Origine.<br>
          Le aree principali di produzione:<br>Chianti Classico, Chianti Colli Senesi e Vernaccia di San Gimignano.
          '
        ],
        [
          'title' => 'Chianti Classico',
          'content' => 'Nella parte a sud del territorio del Chianti Classico, in particolare nei comuni storici di Castellina in Chianti, Gaiole in Chianti, Radda in Chianti e Castelnuovo Berardenga, si trovano 160 ettari di vitigni, collocati ad altitudini variabili tra i 280 ed i 600 metri s.l.m., destinati alla produzione di<br>Chianti Classico DOCG.'

        ],
        [
          'title' => 'CHIANTI COLLI SENESI E VERNACCIA DI SAN GIMIGNANO',
          'content' => 'Sempre nella provincia di Siena, a 250 metri s.l.m., si trovano 165 ettari votati alla produzione di Chianti Colli Senesi DOCG. Ne completano il quadro i circa 60 ettari radicati attorno alla bella cittadina di San Gimignano, destinati alla vinificazione dell’omonima Vernaccia DOCG.'  ]
        ],
        'photos' => $gallery
      ];

      ?>

      <?php include 'commons/stdPage.php'; ?>

      <?php
      shuffle($gallery);
      $pageContent = [
        'index' => 3,
        'superTitle' => 'Mission & Vision',
        'title' => 'CI PRENDIAMO CURA DEI TUOI GUSTI',
        'subtitle' => 'Alla base della filosofia del Geografico pochi, semplici ma chiari concetti: le persone, il territorio e l’identità più vera del suo vino.',
        'description' => [
          [
            'title' => 'Le persone',
            'content' => 'Nonostante tutti i cambiamenti intercorsi nel tempo, la linea guida è rimasta sempre la stessa: l’attenzione alle persone. Per noi è fondamentale garantire ad ogni singolo coltivatore un punto di riferimento sicuro, offrendo supporto agronomico, enologico, produttivo, logistico e finanziario.
            '
          ],
          [
            'title' => 'Il territorio e la sua valorizzazione',
            'content' => 'La terra del Geografico non è solo materia prima e territorialità, intesa come un insieme di luoghi. Questa terra è la protagonista assoluta della nostra identità. Essa ci lega, ci identifica e ci rende orgogliosi di essere Agricoltori del Chianti Geografico. Per tutte queste ragioni, essa viene difesa e, insieme a lei, ne difendiamo il legame con il nostro prodotto storico: il suo vino.'

          ],
          [
            'title' => 'L\'identità del vino',
            'content' => 'Geografico è amore per il vino, per le persone e per il territorio locale. Proprio dal connubio di queste caratteristiche, nasce l’identità stessa del nostro vino'  ]
          ],
          'photos' => $gallery
        ];

        ?>

        <?php include 'commons/stdPage.php'; ?>

        <script type="text/javascript">

        $('#videoWrapper').css({'height':$(window).innerHeight()+'px'})
        $('.geografioMobileNavbar').addClass('d-none');
        window.scrollTo(0, 1);
        $(function(){
          $('.stdPageIconsList li:first-of-type()').remove();
        })
        </script>
