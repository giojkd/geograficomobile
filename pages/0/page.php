<?php

?>

<div id="fullpage">

  <?php foreach($sections as $section){?>
    <div
    onclick="location.replace('?page=<?=($section['index']+1)?>')"
    class="section text-center <?=($section['index'] == $activSectionIndex) ? 'active' : ''?>"
    style="<?=isset($section['background']) ? 'background-size:cover; background-image:url(\''.$imagesPath.$section['background'].'\')' :  ''?>"
    id="section<?=$section['index']?>"
    data-anchor="sectionAnchor<?=$section['index']?>">
    <div class="sectionContentOverlay"></div>
    <div class="sectionContentWrapper d-flex justify-content-center">
      <div class="sectionContent align-self-center">
        <div class="homeSectionHeader">
          <img src="<?=$imagesPath?>geografico-logo.png">
        </div>
        <div class="homeSectionBody">
          <div class=" text-center">
            <h1><?=$section['title']?></h1>
            <h2><?=$section['subtitle']?></h2>
            <p> <a href="?page=<?=($section['index']+1)?>">Scopri <i class="fa fa-angle-right"></i></a></p>
          </div>
        </div>
        <div class="">
          <?php include 'commons/arrowScrollDown.php'; ?>
        </div>
        <div class="homeSectionFooter">
          <div class="sectionCursor">0<?=$section['index']+1?></div>
        </div>
      </div>
    </div>

  </div>
  <?php
}?>
</div>
