<?php
$fullPageCfg = [
 'preIcons' => [

   [
     'icon' => 'add_shopping_cart',
     'title' => 'Shop',
     'href' =>$spsl_default['wine_shop']
   ]
 ],
 'title' => 'Vini & Enoteche',
 'description' => 'Principio ispiratore del lavoro degli agricoltori del Chianti è sempre stato il legame con il territorio e con la sua chiara identità.',

 'buttons' => [
   [
     'title' => 'I nostri vini',
     'icon' => 'grapes-small',
     'href' => '#productsPage'
   ],
   [
     'title' => 'Enoteche',
     'icon' => 'bowl_with_spoon',
     'href' => '#wineShops'
   ],
 ]
];


?>

<?php include 'commons/fullpage.php'; ?>
<?php include 'commons/productsPage.php'; ?>
<?php include 'commons/wineShops.php'; ?>
