<?php
$showStdPageIconLinks = false;
$fullPageCfg = [
  'preIcons' => [
    [
      'icon' => 'activity_history',
      'title' => 'Partnership Piccini',
      'href' =>'#stdPage-1'
    ],
    [
      'icon' => 'bank_building',
      'title' => 'Chi è Banca ifis',
      'href' =>'#stdPage-2'
    ]
  ],
  'title' => 'Progetto di Filiera',
  'description' => '',
  'noArrowDown' => true,
  'buttons' => [
    [
      'title' => 'Scopri',
      'icon' => 'activity_history',
      'href' => ''
    ]
  ]
];
?>

<?php include 'commons/fullpage.php'; ?>
<?php
$pageContent = [
  'index' => 1,
  'superTitle' => 'Partnership',
  'title' => 'LA PARTNERSHIP TRA BANCA IFIS E GEOGRAFICO',
  'subtitle' => 'Banca IFIS e Tenute Piccini hanno definito un rapporto di collaborazione che consente di migliorare i pagamenti dei propri fornitori di uva',
  'description' => [
    [
      'content' => 'Banca IFIS Impresa è la divisione del Gruppo Banca IFIS specializzata nel finanziamento alle PMI. Banca IFIS Impresa ha definito con Tenute Piccini un nuovo rapporto per la gestione dei pagamenti ai propri conferitori fornitori per la consegna dell’uva. L’operazione prevede: la cessione massiva (totale) delle fatture di acconto e saldo (da togliere, non ci sono) derivanti dalla consegna dell’uva al termine della stagione;<br>
      Il Pagamento delle fatture da parte di Banca IFIS  di acconto / saldo al 100% (togliere) alle scadenze concordate.'
    ],
    [
      'content' => 'I vantaggi della collaborazione tra Banca IFIS Impresa e Tenute Piccini impattano tutta la filiera, rendendo immediati e definiti gli incassi dei fornitori e adeguando i tempi di pagamento della cantina al suo ciclo produttivo.'
    ],
    [
      'content' => '
      La collaborazione tra Tenute Piccini e Banca IFIS Impresa crea benefici per tutta la filiera: <br>
      Sicurezza di incasso da parte dei fornitori<br>
      Scadenze di pagamento definite <br>
      Tempi di incasso per i fornitori ridotti rispetto al passato, 30% disponibile già a 30 giorni dalla consegna.'
    ]
  ],
  'photos' => [
    'handshake.png'
  ]
];
?>
<?php include 'commons/stdPage.php'; ?>
<?php

$pageContent = [
  'index' => 2,
  'superTitle' => 'La Banca',
  'title' => 'Chi è Banca Ifis',
  'subtitle' => 'Un universo di opportunità',
  'description' => [
    [
      'content' => 'Il Gruppo Banca IFIS è il più grande operatore indipendente in Italia nel mercato dello specialty finance ed è presente nel settore del credito commerciale a breve, medio e lungo termine e servizi di leasing, in quello dell’acquisizione/dismissione e gestione dei portafogli di crediti non-performing e in quello dei crediti fiscali.'
    ],
  ],
  'photos' => [
    'ifis_chiave.png',
  ]
];
?>
<?php include 'commons/stdPage.php'; ?>
<?php
/*
$pageContent = [
  'index' => 3,
  'superTitle' => 'La Storia',
  'title' => 'La Timeline degli Investimenti',
  'subtitle' => 'Un universo di opportunità',
  'description' => [
    [
      'content' => 'Il Gruppo Banca IFIS è il più grande operatore indipendente in Italia nel mercato dello specialty finance ed è presente nel settore del credito commerciale a breve, medio e lungo termine e servizi di leasing, in quello dell’acquisizione/dismissione e gestione dei portafogli di crediti non-performing e in quello dei crediti fiscali.'
    ],
    [
      'content' => '<img class="img-fluid" src="assets/images/investments-plan.png">'
    ]
  ],
  'photos' => [
    'handshake.png'
  ]
];*/
?>
<?php /* include 'commons/stdPage.php'; */?>
