<?php

$images = 31;
$gallery1 = [];
for($i = 1 ; $i <= $images; $i ++){
  $gallery1[] = 'gallery_vertical/Img_'.$i.'.jpg';
}
shuffle($gallery1);
$fullPageCfg = [

  'title' => 'La culla dei nostri vini',
  'description' => 'Nonostante la capacità di ricevere fino a 60.000 quintali di uva, le storiche cantine di Gaiole in Chianti e San Gimignano, continuano a mantenere la stessa cura e dedizione nella lavorazione affinché l’uva continui ad esprimere il meglio di sé.' ,

  'bottomColumnsExtraClasses' => 'white',
  'bottomColumns' => [
    [
      'title' => 'San Gimignano'
    ],
    [
      'title' => 'Gaiole in Chianti'
    ]
  ]
];


$pageContent = [
  'index' => 1,
  'superTitle' => 'San Gimignano',
  'title' => 'Le eccellenze del chianti classico',
  'subtitle' => 'LA PRESTIGIOSA CANTINA DI SAN GIMIGNANO, IMMERSA NEL VERDE DELLE COLLINE TOSCANE',
  'subtitle' => '',
  'description' => [
    [
      'content' => 'Acquistata nel 1989 per la produzione di Vernaccia di San Gimignano e di Chianti Colli Senesi, ha permesso un’efficace razionalizzazione produttiva. La struttura ha una capacità di vinificazione pari a 10.000 hl in moderni tini di acciaio inox a temperatura controllata, ideali per conservazione degli aromi tipici della Vernaccia. Inoltre, la cantina dispone di un’area dedicata alla produzione ed invecchiamento del Vinsanto in storici caratelli.'
    ]
  ],
  'photos' => $gallery1
];
?>

<?php include 'commons/fullpage.php'; ?>
<?php unset($fullPageCfg['bottomColumnsExtraClasses']) ?>
<?php $linksListBg = ''; ?>
<?php include 'commons/stdPage.php'; ?>
<?php

shuffle($gallery1);
$gallery = [
  'photos' => $gallery1,
  'title' => 'Galleria',
  'index' => 1
];

?>



<?php
shuffle($gallery1);
$pageContent = [
  'index' => 2,
  'superTitle' => 'Gaiole in Chianti',
  'title' => 'La Culla Dei Nostri Vini',
  'title' => 'Le eccellenze del chianti classico',
  'subtitle' => 'LA PRESTIGIOSA CANTINA DI GAIOLE IN CHIANTI, IMMERSA NEL VERDE DELLE COLLINE TOSCANE',
  'subtitle' => '',
  'description' => [
    [
      'content' => 'La storica cantina di Gaiole in Chianti, cuore del Geografico, concepita con tecnologie moderne, è dotata di recipienti di acciaio inox provvisti di sistema automatico per il controllo delle temperature di fermentazione. La cantina di invecchiamento, della capacità complessiva di 7.000 hl, suddivisi fra botti di rovere di media capienza e barriques. Dispone inoltre di una moderna linea di imbottigliamento e confezionamento.'
    ]
  ],
  'photos' => $gallery1
];

?>

<?php include 'commons/stdPage.php'; ?>

<?php include 'commons/fullPageGallery.php' ?>

<?php
/*
$gallery = [
  'photos' => [
    '1.png',
    '1.png',
    '1.png',
  ],
  'title' => 'Gaiole in Chianti',
  'index' => 2
];
*/
?>

<?php /* include 'commons/fullPageGallery.php' */ ?>
