<?php
$page = (isset($_GET['page'])) ? $_GET['page'] : 0 ;
$activSectionIndex = 0;
$imagesPath = 'assets/images/';
$siteUrl = 'https://m.chiantigeografico.it/';
#$siteUrl = 'http://localhost/geograficomobile/';

$spsl_default = [
  'gallery' => $siteUrl.'?page=2#fullPageGallery-1',
  'wine_cards' => $siteUrl.'?page=3#productsPage',
  'wine_shop' => 'https://www.chiantigeografico.it/wineshop',
  'reserve_tasting' => $siteUrl.'?page=3#wineShops',
];

$social_links = [
  'facebook' => 'https://www.facebook.com/agricoltori.geografico/',
  'instagram' => 'https://www.instagram.com/geografico_wines/?hl=it',
  #'youtube' => 'https://www.youtube.com/channel/UCUJuvvubHHagyFhBkd2wXxQ'
];

include 'commons/productsList.php';
include 'commons/sections.php';

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, minimal-ui">
  <meta name="apple-mobile-web-app-capable" content="yes" />

  <!--<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="3d141800-619a-4c5a-8adc-7c3446d8f627" data-blockingmode="auto" type="text/javascript"></script>-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Barlow:100,100i,300,400,400i,500,600,700|Cormorant+Garamond:300,400,500,600,700|Pinyon+Script&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/style.css?v=<?=rand(0,100000)?>">
  <title>Geografico</title>
  <script type="text/javascript">
  var sections = <?=json_encode($sections)?>;
  var imagesPath = '<?=$imagesPath?>';
  </script>
  <link rel="stylesheet" href="assets/js/slick/slick.css">
  <link rel="stylesheet" href="assets/js/slick/slick-theme.css">
  <style media="screen">
  .slidingMenu .slidingMenuContent ul li strong{
    background-image: url('<?=$siteUrl.$imagesPath?>title-pin.png');
  }
  #productsCarouselWrapper{
    /*background-image: url('<?=$siteUrl.$imagesPath?>sfondo_vini_3d.png');*/
    background-color: transparent;
    background-size: 100%;
    background-position: center center;
    background-repeat: no-repeat;
  }
  </style>
  <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/cookie-bar/cookiebar-latest.min.js?theme=flying&always=1&showNoConsent=1&privacyPage=http%3A%2F%2Fchiantigeografico.it%2F"></script>

</head>
<body>

  <?php include 'commons/navbar.php';?>
  <div class="vh-100 vw-100 position-relative">
    <?php include 'pages/'.$page.'/page.php'?>
  </div>

  <?php include 'commons/slidingMenu.php'; ?>
  <?php include 'commons/scrollToTopButton.php'; ?>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <link rel="stylesheet" href="assets/js/fullpage/src/fullpage.css">
  <script src="assets/js/fullpage/src/fullpage.js"></script>
  <script src="assets/js/fullpage/fullpage.resetSliders.min.js"></script>
  <script src="assets/js/fullpage/dist/fullpage.extensions.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" type="text/javascript"></script>
  <script src="assets/js/scripts.js?v=<?=rand(0,100000)?>" type="text/javascript"></script>
  <!--<script id="CookieDeclaration" src="https://consent.cookiebot.com/3d141800-619a-4c5a-8adc-7c3446d8f627/cd.js" type="text/javascript" async></script>-->
</body>
</html>
